<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package KPC
 */

?>

	<!-- Sroll to top -->
	<div class="kpc-scroll-to-top">
		<a href="#">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/scroll-top.png" alt="">
		</a>
	</div>
	<!-- end of scroll to top -->

	</main>

	<!-- Footer -->
	<footer class="kpc-footer">
		<div class="cntr">
			<div class="kpc-footer-cont">
				<div class="gap gap-15 gap-0-xs">
					<div class="md-3 xs-12">
						<a href="<?php bloginfo('url'); ?>" class="kpc-footer-img">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-footer.svg" alt="" class="is-wide">
						</a>
					</div>
					<div class="md-6 xs-12">
						<div class="footer-contact-row">
							<span class="footer-contact-tit">Tel</span>
							<?php
								if(pll_current_language() == 'ja'){
									?>
										<ul class="footer-contact-list">
											<li>
												<a href="tel:+84(0)24-7777- 8788">+84(0)24-7777- 8788</a>
											</li>
											<li>
												<a href="tel:+84(0)96-111-7166">+84(0)96-111-7166（日本人担当）</a>
											</li>
											<li>
												<a href="tel:+84(0)36-6666-959">+84(0)36-6666-959（ベトナム人担当 日本語可）</a>
											</li>
										</ul>
									<?php
								}else if( pll_current_language() == 'en' ){
									?>
										<ul class="footer-contact-list">
											<li>
												<a href="tel:+84(0)24-7777- 8788">+84(0)24-7777- 8788</a>
											</li>
											<li>
												<a href="tel:+84(0)96-111-7166">+84(0)96-111-7166（Japanese representative）</a>
											</li>
											<li>
												<a href="tel:+84(0)36-6666-959">+84(0)36-6666-959（Vietnamese representative [Japanese speaking]）</a>
											</li>
										</ul>
									<?php
								}else{
									?>
										<ul class="footer-contact-list">
											<li>
												<a href="tel:+84(0)24-7777- 8788">+84(0)24-7777- 8788</a>
											</li>
											<li>
												<a href="tel:+84(0)96-111-7166">+84(0)96-111-7166（Đại diện Nhật Bản）</a>
											</li>
											<li>
												<a href="tel:+84(0)36-6666-959">+84(0)36-6666-959（Đại diện Việt Nam [Nói ngôn ngữ Nhật]）</a>
											</li>
										</ul>
									<?php
								}
							?>
						</div>
						<div class="footer-contact-row mb-0">
							<span class="footer-contact-tit">Email</span>
							<ul class="footer-contact-list">
								<li>
									<a href="#">info@kyushupromotioncenter.com</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="md-3 xs-12">
						<ul class="kpc-partner">
							<li>
								<a href="https://thecompany.jp/" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/tc-logo.png" alt="">
								</a>
							</li>
							<li>
								<a href="https://www.ushinokura.com/" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ushino-logo.png" alt="">
								</a>
							</li>
							<li>
								<a href="https://www.kamichiku.co.jp/" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ky-logo.png" alt="">
								</a>
							</li>
							<li>
								<a href="https://www.kyukeiren.or.jp/" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/kyushu-logo.png" alt="">
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="kpc-copyright">
			(C) 2020 KYUSHU PROMOTION CENTER powered by The Company
		</div>
	</footer>
	<!-- End of Footer -->

	<?php wp_footer(); ?>
	<?php 
		$custom_js = get_option( 'theme_js' );
		if(!empty($custom_js)) {
			?>
				<?php echo '<script>'. $custom_js. '</script> '; ?>
			<?php
		}
	?>

	<script>

		$(".scroll-div a").each(function() {
			var $this = $(this);       
			var _href = $this.attr("href"); 
			$this.attr("href",'<?php bloginfo('url'); ?>/' + _href );
		});

	</script>

	</body>
</html>