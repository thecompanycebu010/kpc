<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 * Template Name: Service
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package KPC
 */

get_header();
?>

    <?php

        if( pll_current_language() == 'ja' ){
            ?>
                <!-- Plan -->
                <section class="kpc-plan">
                    <div class="cntr-1024">
                        <div class="kpc-tit kpc-tit-noto">
                            <h3>Plan</h3>
                        </div>
                        <p class="kpc-desc tc mb-60 mb-30-xs">
                            働き方や利用する人数に合わせて選べる<br class="br-sp">様々なプランをご用意しています。<br class="br-pc">
                            月額制のシェアシートプランには法人登記ができるプランもございます。
                        </p>
                        <div class="gap gap-30 gap-0-xs">
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">シェアシートプラン</p>
                                        <p class="kpc-card-plan-price"><span>$120/月</span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00（平日）</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>キャビネット</small>
                                                    </span>
                                                    <span class="plan-list-txt">-</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>法人登記</small>
                                                    </span>
                                                    <span class="plan-list-txt">-</span>
                                                </div>
                                            </li>
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico11.png" alt="">
                                                        <small>会議室</small>
                                                    </span>
                                                    <span class="plan-list-txt">1日1時間無料</span>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">シェアシートプラン（登記対応）</p>
                                        <p class="kpc-card-plan-price"><span>$150/月</span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00（全日）</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>キャビネット</small>
                                                    </span>
                                                    <span class="plan-list-txt">-</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>法人登記</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico11.png" alt="">
                                                        <small>会議室</small>
                                                    </span>
                                                    <span class="plan-list-txt">1日1時間無料</span>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-12 xs-12">
                                <div class="kpc-card v2">
                                    <!-- <p class="kpc-desc t-red tc">
                                        1ヵ月あたり8日間以内のご利用の場合、コワーキングスペースをお得に利用できます
                                    </p> -->
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">フレックスシートプラン（八日間利用）</p>
                                        <p class="kpc-card-plan-price"><span>$50/月</span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00（平日）</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">専用デスクプラン</p>
                                        <p class="kpc-card-plan-price"><span>$200/月</span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00（全日）</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>キャビネット</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>法人登記</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico11.png" alt="">
                                                        <small>会議室</small>
                                                    </span>
                                                    <span class="plan-list-txt">1日1時間無料</span>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">個室オフィスプラン（2〜12名用）</p>
                                        <p class="kpc-card-plan-price"><span>$600〜/月</span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00（全日）</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>キャビネット</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>法人登記</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico11.png" alt="">
                                                        <small>会議室</small>
                                                    </span>
                                                    <span class="plan-list-txt">1日1時間無料</span>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">シェアシートプラン</p>
                                        <p class="kpc-card-plan-price"><span>$120/<small class="price-tag">月</small></span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00（平日）</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <p class="kpc-desc t-red">
                                        まずは無料でお試し!!
                                    </p>
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">シェアシートプラン（登記対応）</p>
                                        <p class="kpc-card-plan-price"><span>$150/<small class="price-tag">月</small></span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00（全日）</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <small>※お1人様につき1回のみご利用いただけます。</small>
                                </div>
                            </div> -->
                            <div class="md-12 xs-12">
                                <div class="kpc-card v2">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">ドロップイン（一時利用）</p>
                                        <p class="kpc-card-plan-price"><span>$8/日</span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00（平日）</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <p class="kpc-desc t-red t-red-under tc">
                                        お1人様に1回限定で、ドロップインを無料でお試しいただけます。
                                    </p>
                                </div>
                            </div>
                            <div class="md-12 xs-12">
                                <div class="kpc-card v3">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">オプションサービス/ 注意事項</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <p class="kpc-desc">
                                                    <strong>プリンター（1枚あたり）</strong><br>
                                                    A4白黒：3,000VND / カラー：6,000 VND <br>
                                                    A3白黒：5,000VND / カラー：10,000 VND  <br>
                                                    <strong>電話受付サービス（月間・年間・期間契約のみ）</strong><br>
                                                    705,000 VND/月（平日8:00-18:00対応）<br>
                                                    <strong>キャビネット（1ヵ月あたり）</strong><br>
                                                    メンバー：470,000VND /メンバー外：588,000VND
                                                </p>
                                            </li>
                                            <li>
                                                <p class="kpc-desc">
                                                    <strong>メンバーアクセスID追加発行（個室プランのみ）</strong><br>
                                                    ID1つにつき2,350,000VND/月<br>
                                                    <strong>ID/鍵再発行 </strong><br>
                                                    ID/鍵1つにつき1,175,000VND <br>
                                                    <strong>駐車場（1ヵ月あたり）</strong><br>
                                                    バイク：100,000VND/車：1,000,000VND
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <p class="kpc-desc t-red t-red-under">
                                        ※受付サービス及びエアコン稼働時間は平日8:00-18:00まで/上記価格は全て税別です/ベトナムドンにてお支払いただきます/上記月額料金は1年契約の場合の価格です/上記プランは2020年6月18日時点でのものです/プラン内容は事前の予告なく変更する場合があります
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="kpc-meeting-event">
                            <div class="kpc-tit kpc-tit-noto">
                                <h3>MEETING ROOM & EVENT SPACE</h3>
                            </div>

                            <!-- view in pc -->
                            <div class="kpc-card v4 v-pc">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">会議室</p>
                                </div>
                                <div class="kpc-card-content">
                                    <div class="kpc-table-responsive">
                                        <table class="kpc-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            会議室
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            A
                                                            <small>（8名用）</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            B
                                                            <small>（4名用）</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            C
                                                            <small>（8名用）</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            うしのくら
                                                            <small>（8名用）</small>
                                                        </h3>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt=""><small>利用料</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$10/h<small>（税別）</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$5/h<small>（税別）</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$10/h<small>（税別）</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$10/h<small>（税別）</small></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt=""><small>利用可能<br>時間</small>
                                                        </div>
                                                    </td>
                                                    <td colspan="3">
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>8:00-18:00（平日）</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>8:00-22:00（全日）</span></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt=""><small>ドリンク</small>
                                                        </div>
                                                    </td>
                                                    <td colspan="3">
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>無料</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>-</span></h3>
                                                        </div>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end of view in pc -->

                            <!-- View in SP -->
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">会議室</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                        A <small>（8名用）</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>利用エリア</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$10/h</span class="plan-bg">（税別）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-18:00（平日）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">無料</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">会議室</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                        B <small>（4名用）</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>利用エリア</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$5/h</span class="plan-bg">（税別）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-18:00（平日）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">無料</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">会議室</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                        C <small>（8名用）</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>利用エリア</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$10/h</span class="plan-bg">（税別）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-18:00（平日）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">無料</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">会議室</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                    うしのくら <small>（8名用）</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>利用エリア</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$10/h</span class="plan-bg">（税別）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-22:00（全日）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">-</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- end of view in sp -->
                            
                            <!-- view pc -->
                            <div class="kpc-card v4 v-pc">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">イベントスペース</p>
                                </div>
                                <div class="kpc-card-content">
                                    <div class="kpc-table-responsive">
                                        <table class="kpc-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                        イベント<br>
                                                        スペース                                             
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            KPCイベントスペース
                                                            <small>（スタンディング：100名/着席：50名）</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            うしのくら展示場
                                                            <small>（スタンディング：60名/着席：30名）</small>
                                                        </h3>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt=""><small>利用料</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$50/h<small>（税別）</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$30/h<small>（税別）</small></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt=""><small>利用可能<br>時間</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>17:00-22:00（平日）、6:00-22:00（全日）</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>6:00-22:00（全日）</span></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt=""><small>ドリンク</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>無料</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>-</span></h3>
                                                        </div>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end view pc -->

                            <!-- view sp -->
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">イベントスペース</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                    KPCイベントスペース<small>（スタンディング：100名/着席：50名）</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>利用エリア</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$50/h</span class="plan-bg">（税別）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">17:00-22:00（平日）、6:00-22:00（全日）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">無料</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">イベントスペース</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                    うしのくら展示場<small>（スタンディング：60名/着席：30名）</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>利用エリア</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$30/h</span class="plan-bg">（税別）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">6:00-22:00（全日）</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">-</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- end view sp -->

                        </div>
                        <ul class="kpc-service-list">
                            <li>上記価格は全て税別です。ベトナムドンにてお支払いただきます。</li>
                            <li>受付サービス及びエアコン稼働時間は平日8:00-18:00までです。</li>
                            <li>ドロップインプラン以外のプランは、別途入会事務手数料（月額利用料の1ヵ月相当額）がかかります。</li>
                            <li>個室オフィスプランは、入居時に別途保証金（月額利用料の1ヵ月相当額）がかかります。</li>
                            <li>家賃・共益費・受付スタッフ・電気・水道・インターネット・ドリンク等込みの料金です。</li>
                            <li>上記月額料金は1年契約の場合の価格です。</li>
                            <li>プラン内容は事前の予告なく変更する場合があります。</li>
                        </ul>
                    </div>
                </section>
                <!-- End of Plan -->
            <?php
        }else if(pll_current_language() == 'en'){
            ?>
                <!-- Plan -->
                <section class="kpc-plan">
                    <div class="cntr-1024">
                        <div class="kpc-tit kpc-tit-noto">
                            <h3>Plans</h3>
                        </div>
                        <p class="kpc-desc tc mb-60 mb-30-xs">
                            
                            We have a range of different plans that <br class="br-sp">fits various working styles and group size.<br class="br-pc">It is possible to apply for an individual Share Seat monthly plan as a corporate body.
                        </p>
                        <div class="gap gap-30 gap-0-xs">
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Share Seat Plan</p>
                                        <p class="kpc-card-plan-price"><span>$120/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00 (Weekdays)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>Cabinet</small>
                                                    </span>
                                                    <span class="plan-list-txt">-</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>Corporate registration</small>
                                                    </span>
                                                    <span class="plan-list-txt">-</span>
                                                </div>
                                            </li>
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico11.png" alt="">
                                                        <small>会議室</small>
                                                    </span>
                                                    <span class="plan-list-txt">1日1時間無料</span>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Share Seat Plan with company registration</p>
                                        <p class="kpc-card-plan-price"><span>$150/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00 (Everyday)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>Cabinet</small>
                                                    </span>
                                                    <span class="plan-list-txt">-</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>Corporate registration</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico11.png" alt="">
                                                        <small>会議室</small>
                                                    </span>
                                                    <span class="plan-list-txt">1日1時間無料</span>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-12 xs-12">
                                <div class="kpc-card v2">
                                    <!-- <p class="kpc-desc t-red tc">
                                        1ヵ月あたり8日間以内のご利用の場合、コワーキングスペースをお得に利用できます
                                    </p> -->
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Flex Seat Plan (8 days)</p>
                                        <p class="kpc-card-plan-price"><span>$50/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00 (Weekdays)</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Dedicated Desk Plan</p>
                                        <p class="kpc-card-plan-price"><span>$200/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00 (Everyday)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>Cabinet</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>Corporate registration</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico11.png" alt="">
                                                        <small>会議室</small>
                                                    </span>
                                                    <span class="plan-list-txt">1日1時間無料</span>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Private Office Room Plan (2-12 people)</p>
                                        <p class="kpc-card-plan-price"><span>$600〜/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00 (Everyday)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>Cabinet</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>Corporate registration</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico11.png" alt="">
                                                        <small>会議室</small>
                                                    </span>
                                                    <span class="plan-list-txt">1日1時間無料</span>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">シェアシートプラン</p>
                                        <p class="kpc-card-plan-price"><span>$120/<small class="price-tag">月</small></span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00（平日）</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <p class="kpc-desc t-red">
                                        まずは無料でお試し!!
                                    </p>
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">シェアシートプラン（登記対応）</p>
                                        <p class="kpc-card-plan-price"><span>$150/<small class="price-tag">月</small></span>（税別）</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>利用可能<br>時間</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00（全日）</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <small>※お1人様につき1回のみご利用いただけます。</small>
                                </div>
                            </div> -->
                            <div class="md-12 xs-12">
                                <div class="kpc-card v2">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Drop-in Plan (1 day)</p>
                                        <p class="kpc-card-plan-price"><span>$8/day</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <!-- <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico08.png" alt="">
                                                        <small>利用エリア</small>
                                                    </span>
                                                    <span class="plan-list-txt">コワーキングスペース</span>
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00 (Weekdays)</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <p class="kpc-desc t-red t-red-under tc">
                                        We offer a one day KPC drop-in trial for free for every one!
                                    </p>
                                </div>
                            </div>
                            <div class="md-12 xs-12">
                                <div class="kpc-card v3">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Optional Services / Remarks</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <p class="kpc-desc">
                                                    <strong>Printing (per page)</strong><br>
                                                    A4 Black and White: 3,000VND/Color: 6,000VND <br>
                                                    A3 Black and White: 5,000VND/Color:10,000VND  <br>
                                                    <strong>Reception/Answering Service (only for monthly, yearly, and periodical plan)</strong><br>
                                                    705,000 VND/month（8:00-18:00 weekdays）<br>
                                                    <strong>Lockers</strong><br>
                                                    470,000VND / month for members, 588,000VND / month for non-members
                                                </p>
                                            </li>
                                            <li>
                                                <p class="kpc-desc">
                                                    <strong>Additional Member Access ID (for Private Office Plan members only)</strong><br>
                                                    2,350,000VND per ID<br>
                                                    <strong>Re-issuing of ID/Key </strong><br>
                                                    1,175,000VND per ID/Key <br>
                                                    <strong>Car Park Space (per month)</strong><br>
                                                    Bike: 100,000VND/Car: 1,000,000VND
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <p class="kpc-desc t-red t-red-under">
                                        ※Reception service and air conditioning is only available during 8:00-18:00 on weekdays. The above prices all exclude tax. Payment to be made in Vietnamese Dong. The above prices are in the case of a one year contract. The above Plan information is valid as of 18th June 2020. Plan details may change without notice.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="kpc-meeting-event">
                            <div class="kpc-tit kpc-tit-noto">
                                <h3>MEETING ROOM & EVENT SPACE</h3>
                            </div>

                            <!-- view in pc -->
                            <div class="kpc-card v4 v-pc">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-content">
                                    <div class="kpc-table-responsive">
                                        <table class="kpc-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            Meeting Room
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            A
                                                            <small>(8 people)</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            B
                                                            <small>(4 people)</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            C
                                                            <small>(8 people)</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            Ushino Kura
                                                            <small>(8 people)</small>
                                                        </h3>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt=""><small>Usage fee</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$10/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$5/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$10/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$10/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt=""><small>Accessibility</small>
                                                        </div>
                                                    </td>
                                                    <td colspan="3">
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>8:00-18:00 (Weekdays)</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>8:00-22:00 (Everyday)</span></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt=""><small>ドリンク</small>
                                                        </div>
                                                    </td>
                                                    <td colspan="3">
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>無料</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>-</span></h3>
                                                        </div>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end of view in pc -->

                            <!-- View in SP -->
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                        A <small>(8 people)</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Fee</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$10/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-18:00(Weekdays)</span>
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">無料</span>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                        B <small>(4 people)</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Fee</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$5/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-18:00(Weekdays)</span>
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">無料</span>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                        C <small>(8 people)</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Fee</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$10/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-18:00(Weekdays)</span>
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">無料</span>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                    Ushino Kura <small>(8 people)</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Fee</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$10/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-22:00(Everyday)</span>
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">-</span>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                            <!-- end of view in sp -->
                            
                            <!-- view pc -->
                            <div class="kpc-card v4 v-pc">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Event Space</p>
                                </div>
                                <div class="kpc-card-content">
                                    <div class="kpc-table-responsive">
                                        <table class="kpc-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                        Event<br>
                                                        Space                                             
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            KPC Event Space
                                                            <small>( Capacity: 100 Standing / 50 Seated )</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            Ushino Kura Showroom
                                                            <small>( Capacity: 60 Standing / 30 Seated )</small>
                                                        </h3>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt=""><small>Usage Cost</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$50/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$30/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt=""><small>Accessibility</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>17:00-22:00(Weekdays)、6:00-22:00(Everyday)</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>6:00-22:00(Everyday)</span></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt=""><small>ドリンク</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>無料</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>-</span></h3>
                                                        </div>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end view pc -->

                            <!-- view sp -->
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Event Space</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                    KPC Event Space<small>( Capacity: 100 Standing / 50 Seated )</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Cost</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$50/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">17:00-22:00(Weekdays)、6:00-22:00(Everyday)</span>
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">無料</span>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Event Space</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                    Ushino Kura Showroom<small>( Capacity: 60 Standing / 30 Seated )</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Cost</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$30/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">6:00-22:00(Everyday)</span>
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>利用可能<br>時間</small>
                                                </span>
                                                <span class="plan-list-txt">-</span>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                            <!-- end view sp -->

                        </div>
                        <ul class="kpc-service-list">
                            <li>The above prices all exclude tax. Payment to be made in Vietnamese Dong.</li>
                            <li>Reception service and air conditioning is only available during 8:00-18:00 on weekdays.</li>
                            <li>All plans (excluding the Drop-in Plan) require a separate registration fee, equivalent of one month's worth of the chosen plan.</li>
                            <li>The Private Office Plan requires a deposit, equivalent of one month's worth of fee at time of moving in.</li>
                            <li>Rent, common area usage fees, reception staff, utilities (electricity and water), internet, drink are included in plan prices.</li>
                            <li>The above prices are in the case of a one year contract.</li>
                            <li>Plan details may change without notice.</li>
                        </ul>
                    </div>
                </section>
                <!-- End of Plan -->
            <?php
        }else{
            ?>
                <!-- Plan -->
                <section class="kpc-plan">
                    <div class="cntr-1024">
                        <div class="kpc-tit kpc-tit-noto">
                            <h3>CÁC GÓI DỊCH VỤ</h3>
                        </div>
                        <p class="kpc-desc tc mb-60 mb-30-xs">
                            
                            Chúng tôi có các kế hoạch khác nhau phục vụ <br class="br-sp">cho nhiều phong cách làm việc và quy mô nhóm.<br class="br-pc">
                            Có thể đăng ký một số gói chia sẻ hàng tháng với tư cách là một cơ quan công ty.
                        </p>
                        <div class="gap gap-30 gap-0-xs">
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Share Seat Plan</p>
                                        <p class="kpc-card-plan-price"><span>$120/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00 (Weekdays)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>Cabinet</small>
                                                    </span>
                                                    <span class="plan-list-txt">-</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>Corporate registration</small>
                                                    </span>
                                                    <span class="plan-list-txt">-</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Share Seat Plan with company registration</p>
                                        <p class="kpc-card-plan-price"><span>$150/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00 (Everyday)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>Cabinet</small>
                                                    </span>
                                                    <span class="plan-list-txt">-</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>Corporate registration</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-12 xs-12">
                                <div class="kpc-card v2">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Flex Seat Plan (8 days)</p>
                                        <p class="kpc-card-plan-price"><span>$50/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00 (Weekdays)</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Dedicated Desk Plan</p>
                                        <p class="kpc-card-plan-price"><span>$200/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00 (Everyday)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>Cabinet</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>Corporate registration</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-6 xs-12">
                                <div class="kpc-card">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Private Office Room Plan (2-12 people)</p>
                                        <p class="kpc-card-plan-price"><span>$600〜/month</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">6:00-22:00 (Everyday)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico12.png" alt="">
                                                        <small>Cabinet</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico10.png" alt="">
                                                        <small>Corporate registration</small>
                                                    </span>
                                                    <span class="plan-list-txt">○</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="md-12 xs-12">
                                <div class="kpc-card v2">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Drop-in Plan (1 day)</p>
                                        <p class="kpc-card-plan-price"><span>$8/day</span>(excluding tax)</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <div class="plan-list-row">
                                                    <span class="plan-list-ico">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico09.png" alt="">
                                                        <small>Accessibility</small>
                                                    </span>
                                                    <span class="plan-list-txt">8:00-18:00 (Weekdays)</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <p class="kpc-desc t-red t-red-under tc">
                                        We offer a one day KPC drop-in trial for free for every one!
                                    </p>
                                </div>
                            </div>
                            <div class="md-12 xs-12">
                                <div class="kpc-card v3">
                                    <div class="kpc-card-head">
                                        <p class="kpc-card-plan-txt">Dịch vụ kèm theo</p>
                                    </div>
                                    <div class="kpc-card-content">
                                        <ul class="kpc-card-plan-list">
                                            <li>
                                                <p class="kpc-desc">
                                                    <strong>In ấn (mỗi trang)</strong><br>
                                                    A4 đen trắng: 3,000VND/Màu: 6,000VND <br>
                                                    A3 đen trắng: 5,000VND/Màu: 10,000VND  <br>
                                                    <strong>Dịch vụ Tiếp tân/Trả lời điện thoại (chỉ theo hợp đồng hàng tháng, hàng năm hoặc theo khung thời gian ấn định khác)</strong><br>
                                                    705,000 VND/month（8:00-18:00 weekdays）<br>
                                                    <strong>Sử dụng tủ có khóa (mỗi tháng)</strong><br>
                                                    Có dùng gói: 470,000VNĐ /Không dùng gói: 588,000VNĐ
                                                </p>
                                            </li>
                                            <li>
                                                <p class="kpc-desc">
                                                    <strong>Bổ sung quyền truy cập ID thành viên  (chỉ dành cho thành viên Gói văn phòng riêng)</strong><br>
                                                    2.350.000VNĐ mỗi ID<br>
                                                    <strong>Cấp lại ID / Khóa </strong><br>
                                                    1,175,000VNĐ mỗi ID / Key<br>
                                                    <strong>Phí đỗ xe (mỗi tháng)</strong><br>
                                                    Xe máy: 100,000VND/Ô tô: 1,000,000VND
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <p class="kpc-desc t-red t-red-under">
                                        ※Dịch vụ lễ tân và máy lạnh chỉ được cung cấp từ 8:00-18:00 vào các ngày trong tuần. Giá trên đều chưa bao gồm thuế. Thanh toán bằng tiền Việt Nam đồng. Giá trên là trong trường hợp hợp đồng một năm. Thông tin về Các gói dịch vụ ở trên có hiệu lực kể từ ngày 18 tháng 6 năm 2020. Chi tiết về Các gói dịch vụ có thể thay đổi mà không cần thông báo trước.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="kpc-meeting-event">
                            <div class="kpc-tit kpc-tit-noto">
                                <h3>MEETING ROOM & EVENT SPACE</h3>
                            </div>

                            <!-- view in pc -->
                            <div class="kpc-card v4 v-pc">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-content">
                                    <div class="kpc-table-responsive">
                                        <table class="kpc-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            Meeting Room
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            A
                                                            <small>(8 people)</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            B
                                                            <small>(4 people)</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            C
                                                            <small>(8 people)</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            Ushino Kura
                                                            <small>(8 people)</small>
                                                        </h3>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt=""><small>Usage fee</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$10/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$5/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$10/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$10/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt=""><small>Accessibility</small>
                                                        </div>
                                                    </td>
                                                    <td colspan="3">
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>8:00-18:00 (Weekdays)</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>8:00-22:00 (Everyday)</span></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end of view in pc -->

                            <!-- View in SP -->
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                        A <small>(8 people)</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Fee</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$10/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-18:00(Weekdays)</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                        B <small>(4 people)</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Fee</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$5/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-18:00(Weekdays)</span>
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico15.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">無料</span>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                        C <small>(8 people)</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Fee</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$10/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-18:00(Weekdays)</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Meeting Room</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                    Ushino Kura <small>(8 people)</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Fee</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$10/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">8:00-22:00(Everyday)</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- end of view in sp -->
                            
                            <!-- view pc -->
                            <div class="kpc-card v4 v-pc">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Event Space</p>
                                </div>
                                <div class="kpc-card-content">
                                    <div class="kpc-table-responsive">
                                        <table class="kpc-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                        Event<br>
                                                        Space                                             
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            KPC Event Space
                                                            <small>( Capacity: 100 Standing / 50 Seated )</small>
                                                        </h3>
                                                    </th>
                                                    <th>
                                                        <h3 class="kpc-table-head-tit">
                                                            Ushino Kura Showroom
                                                            <small>( Capacity: 60 Standing / 30 Seated )</small>
                                                        </h3>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt=""><small>Usage Cost</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$50/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3>$30/h<small>(excluding tax)</small></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="kpc-table-ico kpc-table-padd">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt=""><small>Accessibility</small>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>17:00-22:00(Weekdays)、6:00-22:00(Everyday)</span></h3>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="kpc-table-padd tc">
                                                            <h3><span>6:00-22:00(Everyday)</span></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end view pc -->

                            <!-- view sp -->
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Event Space</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                    KPC Event Space<small>( Capacity: 100 Standing / 50 Seated )</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Cost</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$50/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">17:00-22:00(Weekdays)、6:00-22:00(Everyday)</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="kpc-card v4 v-sp">
                                <div class="kpc-card-head">
                                    <p class="kpc-card-plan-txt">Event Space</p>
                                </div>
                                <div class="kpc-card-head-2">
                                    <p class="kpc-card-plan-txt-2">
                                    Ushino Kura Showroom<small>( Capacity: 60 Standing / 30 Seated )</small>
                                    </p>
                                </div>
                                <div class="kpc-card-content">
                                    <ul class="kpc-card-plan-list">
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico13.png" alt="">
                                                    <small>Usage Cost</small>
                                                </span>
                                                <span class="plan-list-txt"><span class="plan-bg">$30/h</span class="plan-bg">(excluding tax)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="plan-list-row">
                                                <span class="plan-list-ico">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico14.png" alt="">
                                                    <small>Accessibility</small>
                                                </span>
                                                <span class="plan-list-txt">6:00-22:00(Everyday)</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- end view sp -->

                        </div>
                        <ul class="kpc-service-list">
                            <li>Giá trên đều chưa bao gồm thuế. Thanh toán bằng đồng tiền Việt Nam.</li>
                            <li>Dịch vụ lễ tân và máy lạnh chỉ hoạt động từ 8:00-18:00 các ngày trong tuần.</li>
                            <li>Tất cả các gói (không bao gồm "Gói 1 ngày") yêu cầu một khoản phí đăng ký riêng, tương đương với giá trị một tháng của gói đã chọn.</li>
                            <li>"Gói Văn phòng riêng" yêu cầu một khoản đặt cọc, tương đương với một tháng phí tại thời điểm chuyển đến.</li>
                            <li>Giá thuê, phí sử dụng khu vực chung, nhân viên lễ tân, tiện ích (điện nước), internet, đồ uống đã bao gồm trong giá gói.</li>
                            <li>Giá trên áp dụng cho hợp đồng 1 năm</li>
                            <li>Chi tiết gói có thể thay đổi mà không cần báo trước</li>
                        </ul>
                    </div>
                </section>
                <!-- End of Plan -->
            <?php
        }

    ?>

<?php
get_footer();