<?php
/**
 * The template for displaying confirm pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package KPC
 */

get_header();
?>

    <!-- Contact -->
    <section class="kpc-contact" id="kpc-contact">
        <div class="cntr-960">
            <div class="kpc-tit tit-white">
                <h3>Contact</h3>
            </div>
            <p class="kpc-desc tc">
                このフォームを使用するには、プライバシーポリシーに同意して内容を送信してください。
            </p>
            <?php echo do_shortcode('[mwform_formkey key="24"]'); ?>
        </div>
    </section>
    <!-- End of Contact -->

<?php
get_footer();