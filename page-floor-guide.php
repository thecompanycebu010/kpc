<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 * Template Name: Floor Guide
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package KPC
 */

get_header();
?>

    <?php
        if(pll_current_language() == 'ja'){
            ?>
                <!-- Floor Guide -->
                <section class="kpc-floor-guide">
                    <div class="cntr-960">
                        <div class="kpc-tit kpc-tit-noto kpc-fs">
                            <h3>K.P.C.（九州プロモーションセンター）powered by The Company 棟</h3>
                        </div>
                        <div class="kpc-my-tabs" id="kpc_my_tab1">
                            <ul class="kpc-tabs">
                                <li class="kpc-tab-link" data-target="#tab1">
                                    <a>1F</a>
                                </li>
                                <li class="kpc-tab-link" data-target="#tab2">
                                    <a>2F</a>
                                </li>
                                <li class="kpc-tab-link" data-target="#tab3">
                                    <a>3F</a>
                                </li>
                            </ul>
                            <div class="kpc-tab-box">
                                <div id="tab1" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image01.png" alt="" class="is-wide">
                                </div>
                                <div id="tab2" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image02.png" alt="" class="is-wide">
                                </div>
                                <div id="tab3" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image03.png" alt="" class="is-wide">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kpc-space">
                        <div class="kpc-tit kpc-tit-noto kpc-fs kpc-spc">
                            <h3>Space</h3>
                        </div>
                        <div class="kpc-slider-box">
                            <div id="kpc-space-slider" class="kpc-space-slider owl-carousel owl-theme">
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide01.jpg" alt="">
                                    <p>受付</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide02.jpg" alt="">
                                    <p>コワーキングスペース</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide03.jpg" alt="">
                                    <p>テラス</p>
                                </div>
                            </div>
                            <div class="kpc-slider-counter"></div>
                        </div>
                    </div>
                    <div class="cntr-960">
                        <div class="kpc-tit kpc-tit-noto kpc-fs">
                            <h3>K.P.C.（九州プロモーションセンター）和牛ダイニングうしのくら・ショールーム棟</h3>
                        </div>
                        <div class="kpc-my-tabs" id="kpc_my_tab2">
                            <ul class="kpc-tabs">
                                <li class="kpc-tab-link" data-target="#tab4">
                                    <a>1F</a>
                                </li>
                                <li class="kpc-tab-link" data-target="#tab5">
                                    <a>3F</a>
                                </li>
                            </ul>
                            <div class="kpc-tab-box">
                                <div id="tab4" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image04.png" alt="" class="is-wide">
                                </div>
                                <div id="tab5" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image05.png" alt="" class="is-wide">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kpc-space">
                        <div class="kpc-tit kpc-tit-noto kpc-fs kpc-spc">
                            <h3>Space</h3>
                        </div>
                        <div class="kpc-slider-box">
                            <div id="kpc-space-slider-2" class="kpc-space-slider owl-carousel owl-theme">
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide04.jpg" alt="">
                                    <p>ガーデン</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide05.jpg" alt="">
                                    <p>ショールーム</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide06.jpg" alt="">
                                    <p>会議室</p>
                                </div>
                            </div>
                            <div class="kpc-slider-counter-2"></div>
                        </div>
                    </div>
                </section>
                <!-- End of Floor Guide -->
            <?php
        }else if( pll_current_language() == 'en' ){
            ?>
                <!-- Floor Guide -->
                <section class="kpc-floor-guide">
                    <div class="cntr-960">
                        <div class="kpc-tit kpc-tit-noto kpc-fs">
                            <h3>K.P.C.（Kyushu Promotion Center）powered by The Company Bldg.</h3>
                        </div>
                        <div class="kpc-my-tabs" id="kpc_my_tab1">
                            <ul class="kpc-tabs">
                                <li class="kpc-tab-link" data-target="#tab1">
                                    <a>1F</a>
                                </li>
                                <li class="kpc-tab-link" data-target="#tab2">
                                    <a>2F</a>
                                </li>
                                <li class="kpc-tab-link" data-target="#tab3">
                                    <a>3F</a>
                                </li>
                            </ul>
                            <div class="kpc-tab-box">
                                <div id="tab1" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image01.png" alt="" class="is-wide">
                                </div>
                                <div id="tab2" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image02.png" alt="" class="is-wide">
                                </div>
                                <div id="tab3" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image03.png" alt="" class="is-wide">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kpc-space">
                        <div class="kpc-tit kpc-tit-noto kpc-fs kpc-spc">
                            <h3>Space</h3>
                        </div>
                        <div class="kpc-slider-box">
                            <div id="kpc-space-slider" class="kpc-space-slider owl-carousel owl-theme">
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide01.jpg" alt="">
                                    <p>Reception</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide02.jpg" alt="">
                                    <p>Coworking Space</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide03.jpg" alt="">
                                    <p>Terrace</p>
                                </div>
                            </div>
                            <div class="kpc-slider-counter"></div>
                        </div>
                    </div>
                    <div class="cntr-960">
                        <div class="kpc-tit kpc-tit-noto kpc-fs">
                            <h3>K.P.C.（Kyushu Promotion Center）Wagyu Dining Ushino Kura / Showroom Bldg.</h3>
                        </div>
                        <div class="kpc-my-tabs" id="kpc_my_tab2">
                            <ul class="kpc-tabs">
                                <li class="kpc-tab-link" data-target="#tab4">
                                    <a>1F</a>
                                </li>
                                <li class="kpc-tab-link" data-target="#tab5">
                                    <a>3F</a>
                                </li>
                            </ul>
                            <div class="kpc-tab-box">
                                <div id="tab4" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image04.png" alt="" class="is-wide">
                                </div>
                                <div id="tab5" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image05.png" alt="" class="is-wide">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kpc-space">
                        <div class="kpc-tit kpc-tit-noto kpc-fs kpc-spc">
                            <h3>Space</h3>
                        </div>
                        <div class="kpc-slider-box">
                            <div id="kpc-space-slider-2" class="kpc-space-slider owl-carousel owl-theme">
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide04.jpg" alt="">
                                    <p>Garden</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide05.jpg" alt="">
                                    <p>Showroom</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide06.jpg" alt="">
                                    <p>Meeting Room</p>
                                </div>
                            </div>
                            <div class="kpc-slider-counter-2"></div>
                        </div>
                    </div>
                </section>
                <!-- End of Floor Guide -->
            <?php
        }else{
            ?>
                <!-- Floor Guide -->
                <section class="kpc-floor-guide">
                    <div class="cntr-960">
                        <div class="kpc-tit kpc-tit-noto kpc-fs">
                            <h3>K.P.C.（Kyushu Promotion Center）powered by The Company Bldg.</h3>
                        </div>
                        <div class="kpc-my-tabs" id="kpc_my_tab1">
                            <ul class="kpc-tabs">
                                <li class="kpc-tab-link" data-target="#tab1">
                                    <a>1F</a>
                                </li>
                                <li class="kpc-tab-link" data-target="#tab2">
                                    <a>2F</a>
                                </li>
                                <li class="kpc-tab-link" data-target="#tab3">
                                    <a>3F</a>
                                </li>
                            </ul>
                            <div class="kpc-tab-box">
                                <div id="tab1" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image01.png" alt="" class="is-wide">
                                </div>
                                <div id="tab2" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image02.png" alt="" class="is-wide">
                                </div>
                                <div id="tab3" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image03.png" alt="" class="is-wide">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kpc-space">
                        <div class="kpc-tit kpc-tit-noto kpc-fs kpc-spc">
                            <h3>Space</h3>
                        </div>
                        <div class="kpc-slider-box">
                            <div id="kpc-space-slider" class="kpc-space-slider owl-carousel owl-theme">
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide01.jpg" alt="">
                                    <p>Reception</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide02.jpg" alt="">
                                    <p>Coworking Space</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide03.jpg" alt="">
                                    <p>Terrace</p>
                                </div>
                            </div>
                            <div class="kpc-slider-counter"></div>
                        </div>
                    </div>
                    <div class="cntr-960">
                        <div class="kpc-tit kpc-tit-noto kpc-fs">
                            <h3>K.P.C.（Kyushu Promotion Center）Wagyu Dining Ushino Kura / Showroom Bldg.</h3>
                        </div>
                        <div class="kpc-my-tabs" id="kpc_my_tab2">
                            <ul class="kpc-tabs">
                                <li class="kpc-tab-link" data-target="#tab4">
                                    <a>1F</a>
                                </li>
                                <li class="kpc-tab-link" data-target="#tab5">
                                    <a>3F</a>
                                </li>
                            </ul>
                            <div class="kpc-tab-box">
                                <div id="tab4" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image04.png" alt="" class="is-wide">
                                </div>
                                <div id="tab5" class="kpc-tab-content">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/image05.png" alt="" class="is-wide">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kpc-space">
                        <div class="kpc-tit kpc-tit-noto kpc-fs kpc-spc">
                            <h3>Space</h3>
                        </div>
                        <div class="kpc-slider-box">
                            <div id="kpc-space-slider-2" class="kpc-space-slider owl-carousel owl-theme">
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide04.jpg" alt="">
                                    <p>Garden</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide05.jpg" alt="">
                                    <p>Showroom</p>
                                </div>
                                <div class="kpc-space-slider-item item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/floor-guide/slide06.jpg" alt="">
                                    <p>Meeting Room</p>
                                </div>
                            </div>
                            <div class="kpc-slider-counter-2"></div>
                        </div>
                    </div>
                </section>
                <!-- End of Floor Guide -->
            <?php
        }
    ?>

<?php
get_footer();