<?php
/**
 * The template for displaying all pages
 * Template Name: Homepage
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package KPC
 */

get_header();
?>

    <?php

        if( pll_current_language() == 'ja' ){
            ?>
                <!-- News -->
                <section class="kpc-news">
                    <div class="cntr-1170">
                        <div class="kpc-news-row">
                            <div class="kpc-tit">
                                <h3>News</h3>
                            </div>
                            <div class="kpc-news-cont">
                                <?php
                                // news query
                                $news_query = new WP_Query(array('post_type'=>'news', 'post_status'=>'publish', 'posts_per_page'=>1, 'order'=> 'DESC')); ?>
                                <?php if ( $news_query->have_posts() ) : ?>

                                    <?php while ( $news_query->have_posts() ) : $news_query->the_post(); ?>
                                    <div class="kpc-news-left">
                                        <span class="kpc-news-date"><?php the_date('Y.m.d'); ?></span>
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>">Read More</a>
                                    <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                                <?php else : ?>
                                    <h4 class="cc-no-post tc"><?php _e( 'Coming Soon...' ); ?></h4>
                                <?php endif; ?>
                                
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of News -->

                <!-- Concept -->
                <section class="kpc-concept" id="kpc-concept">
                    <div class="cntr-1170">
                        <div class="kpc-tit">
                            <h3>Concept</h3>
                        </div>
                        <div class="kpc-con-txt">
                            <h4>
                                九州とハノイを繋ぐ、<br>
                                オール九州のシンボルタワー
                            </h4>
                        </div>
                        <div class="kpc-con-cont">
                            <p>
                                九州の情報発信や、現地のビジネス情報収集の拠点となる「九州プロモーションセンター powered by The Company」が11月2日にオープンします。<br><br>
                                (一社)九州経済連合会(以下:九経連)の協力の下、カミチクグループ(鹿児島市)のKAMICHIKU VIETNAM JOINT STOCK COMPANYが、ベトナム外務省との契約主体となり、「The Company」ブランドでシェアオフィスを世界展開する(株)Zero-Ten Park(福岡市)とコラボレーション。<br><br>
                                ハノイの中心であるキンマーエリアに、ベトナム外務省所管の国有施設2棟で構成され、九州と九州に関わりのある企業・団体が入居する、オール九州のシンボルタワーです。<br><br>
                                コワーキングスペースやイベント、その他コミュニティスペースを通して、九州 - ベトナム間の交流に関わる人とのネットワークやコミュニティの形成を目指します。<br><br>
                                九経連による、九州企業のベトナム計画投資省と総領事館へのコネクションも 活用できます。
                            </p>
                        </div>
                        <div class="kpc-con-circ">
                            <div class="kpc-con-circ-row">
                                <div class="kpc-con-circ-md">
                                    <div class="kpc-con-box">
                                        <div class="kpc-con-img">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico01.svg" alt="">
                                        </div>
                                        <h4 class="kpc-con-tit">Project</h4>
                                        <p>
                                            プロジェクト創生型<br>
                                            ワークスペース
                                        </p>
                                    </div>
                                </div>
                                <div class="kpc-con-circ-md">
                                    <div class="kpc-con-box">
                                        <div class="kpc-con-img">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico02.svg" alt="">
                                        </div>
                                        <h4 class="kpc-con-tit">Network</h4>
                                        <p>
                                            豊富なネットワークで<br>
                                            活動をサポート
                                        </p>
                                    </div>
                                </div>
                                <div class="kpc-con-circ-md">
                                    <div class="kpc-con-box">
                                        <div class="kpc-con-img">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico03.svg" alt="">
                                        </div>
                                        <h4 class="kpc-con-tit">Connection</h4>
                                        <p>
                                            九州に関わる様々な<br>
                                            企業・団体との繋がり
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of Concept -->

                <!-- Service -->
                <section class="kpc-service">
                    <div class="kpc-float-box1"></div>
                    <div class="kpc-float-box2"></div>
                    <div class="kpc-float-box3"></div>
                    <div class="kpc-float-box4"></div>
                    <div class="kpc-float-box5"></div>
                    <div class="cntr-1170">
                        <div class="kpc-service-box">
                            <div class="kpc-float-box6"></div>
                            <div class="kpc-float-box7"></div>
                            <div class="kpc-tit">
                                <h3>Service</h3>
                            </div>
                            <p class="kpc-desc tc">
                                働き方や利用する人数に合わせて選べる様々なプランをご用意しています。<br>
                                月額制のシェアシートプランには法人登記ができるプランもございます。<br><br>

                                会員様が無料でご利用いただける便利なサービスに加え、<br>
                                様々なオプションサービスをご用意しています。
                            </p>
                            <div class="kpc-tit-jp">
                                <h4>全プラン共通サービス</h4>
                            </div>
                            <div class="kpc-serv-plan-box">
                                <div class="kpc-plan-row">
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico04.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">無料ドリンク</p>
                                        </div>
                                    </div>
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico05.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">高速インターネット</p>
                                        </div>
                                    </div>
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico06.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">フォンブース利用</p>
                                        </div>
                                    </div>
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico07.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">
                                                マルチロケーション<br>
                                                （九経連会員様限定）
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="kpc-btn">
                                    <a href="<?php bloginfo('url'); ?>/service">PLAN LIST</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of Service -->

                <!-- Members -->
                <section class="kpc-members" id="kpc-member">
                    <div class="cntr">
                        <div class="kpc-tit">
                            <h3>Members</h3>
                        </div>
                        <p class="kpc-desc tc">
                            九州プロモーションセンター powered by The Companyの<br>
                            メンバーを紹介します。
                        </p>
                        <div class="kpc-btn">
                            <a href="#">Coming Soon</a>
                        </div>
                    </div>
                </section>
                <!-- End of Members -->

                <!-- Contact -->
                <section class="kpc-contact" id="kpc-contact">
                    <div class="cntr-960">
                        <div class="kpc-tit tit-white">
                            <h3>Contact</h3>
                        </div>
                        <p class="kpc-desc tc">
                            必要事項をご記⼊いただき、「CHECK」へ進んでください。<br>
                            このフォームを使用するには、プライバシーポリシーに同意いただく必要があります。
                        </p>
                        <?php echo do_shortcode('[mwform_formkey key="24"]'); ?>
                    </div>
                </section>
                <!-- End of Contact -->

                <!-- Access -->
                <section class="kpc-access">
                    <div class="cntr-960">
                        <div class="kpc-tit">
                            <h3>Access</h3>
                        </div>
                        <p class="kpc-desc tc">
                            <strong>K.P.C.（九州プロモーションセンター）powered by The Company 棟</strong><br>
                            Villa A3, 73 Van Bao, Ngoc Khanh, Ba Dinh, Ha Noi, Vietnam<br><br>

                            <strong>K.P.C.（九州プロモーションセンター）和牛ダイニングうしのくら・ショールーム棟</strong><br>
                            44 Van Bao, Ngoc Khanh, Ba Dinh, Ha Noi, Vietnam
                        </p>
                        <div class="kpc-access-map">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/access/img01.jpg" alt="" class="is-wide">
                        </div>
                    </div>
                </section>
                <!-- End of Access -->
            <?php
        } else if( pll_current_language() == 'en' ){
            ?>
                <!-- News -->
                <section class="kpc-news">
                    <div class="cntr-1170">
                        <div class="kpc-news-row">
                            <div class="kpc-tit">
                                <h3>News</h3>
                            </div>
                            <div class="kpc-news-cont">
                                <?php
                                // news query
                                $news_query = new WP_Query(array('post_type'=>'news', 'post_status'=>'publish', 'posts_per_page'=>1, 'order'=> 'DESC')); ?>
                                <?php if ( $news_query->have_posts() ) : ?>

                                    <?php while ( $news_query->have_posts() ) : $news_query->the_post(); ?>
                                    <div class="kpc-news-left">
                                        <span class="kpc-news-date"><?php the_date('Y.m.d'); ?></span>
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>">Read More</a>
                                    <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                                <?php else : ?>
                                    <h4 class="cc-no-post tc"><?php _e( 'Coming Soon...' ); ?></h4>
                                <?php endif; ?>
                                
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of News -->

                <!-- Concept -->
                <section class="kpc-concept" id="kpc-concept">
                    <div class="cntr-1170">
                        <div class="kpc-tit">
                            <h3>Concept</h3>
                        </div>
                        <div class="kpc-con-txt">
                            <h4>
                                The "All Kyushu Symbol Tower"<br>
                                that connects Kyushu and Hanoi
                            </h4>
                        </div>
                        <div class="kpc-con-cont">
                            <p>
                              Kyushu Promotion Center powered by The Company, an information and networking space for Kyushu related people and companies, is scheduled to open on 2nd November 2020.<br><br>
                              With the cooperation of the Kyushu Economic Federation, Kamichiku Group company KAMICHIKU VIETNAM JOINT STOCK COMPANY has contracted with Vietnam Ministry of Foreign Affairs. This partnership has led to the collaboration with 'The Company', a brand by global share-office operator Zero-Ten Park Co., Ltd.<br><br>
                              The "All Kyushu Symbol Tower" Kyushu Promotion Center is located in the Kim Ma area of central Hanoi, consists of two government owned facilities under Ministry of Foreign Affairs Vietnam jurisdiction.<br><br>
                              With the use of our co-working space, events, and through our community, we aim to form and facilitate communities to exchange information and networks between Kyushu region of Japan and Vietnam.<br><br>
                              With the close support by the Kyushu Economic Federation, we are able to share their connection with the Ministry of Planning and Investment of Vietnam and Consulate-General of the Socialist Republic of Vietnam in Fukuoka.
                            </p>
                        </div>
                        <div class="kpc-con-circ">
                            <div class="kpc-con-circ-row">
                                <div class="kpc-con-circ-md">
                                    <div class="kpc-con-box">
                                        <div class="kpc-con-img">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico01.svg" alt="">
                                        </div>
                                        <h4 class="kpc-con-tit">Project</h4>
                                        <p>
                                            Cultivated workspace for<br>
                                            project development
                                        </p>
                                    </div>
                                </div>
                                <div class="kpc-con-circ-md">
                                    <div class="kpc-con-box">
                                        <div class="kpc-con-img">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico02.svg" alt="">
                                        </div>
                                        <h4 class="kpc-con-tit">Network</h4>
                                        <p>
                                            Expansive network supporting<br>
                                            projects and businesses
                                        </p>
                                    </div>
                                </div>
                                <div class="kpc-con-circ-md">
                                    <div class="kpc-con-box">
                                        <div class="kpc-con-img">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico03.svg" alt="">
                                        </div>
                                        <h4 class="kpc-con-tit">Connection</h4>
                                        <p>
                                            Extensive connections with companies<br>
                                            and organisations associated with Kyushu
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of Concept -->

                <!-- Service -->
                <section class="kpc-service">
                    <div class="kpc-float-box1"></div>
                    <div class="kpc-float-box2"></div>
                    <div class="kpc-float-box3"></div>
                    <div class="kpc-float-box4"></div>
                    <div class="kpc-float-box5"></div>
                    <div class="cntr-1170">
                        <div class="kpc-service-box">
                            <div class="kpc-float-box6"></div>
                            <div class="kpc-float-box7"></div>
                            <div class="kpc-tit">
                                <h3>Service</h3>
                            </div>
                            <p class="kpc-desc tc">
                                We have a range of different plans that fits various working styles and group size.<br>
                                It is possible to apply for an individual Share Seat monthly plan as a corporate body.<br><br>

                                In addition to the free services we offer to our members, <br>
                                we also offer optional services for your additional needs.
                            </p>
                            <div class="kpc-tit-jp">
                                <h4>Services available to all plans</h4>
                            </div>
                            <div class="kpc-serv-plan-box">
                                <div class="kpc-plan-row">
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico04.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">Free drinks</p>
                                        </div>
                                    </div>
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico05.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">Fast Wi-Fi</p>
                                        </div>
                                    </div>
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico06.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">Private booths designed for phone call usage.</p>
                                        </div>
                                    </div>
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico07.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">
                                                Access to The Company offices worldwide<br>
                                                (Kyushu Economic Federation members only)
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="kpc-btn">
                                    <a href="<?php bloginfo('url'); ?>/service-en">PLAN LIST</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of Service -->

                <!-- Members -->
                <section class="kpc-members" id="kpc-member">
                    <div class="cntr">
                        <div class="kpc-tit">
                            <h3>MEMBERS</h3>
                        </div>
                        <p class="kpc-desc tc">
                            Introducing our Kyushu Promotion Center powered by The Company members!
                        </p>
                        <div class="kpc-btn">
                            <a href="#">Coming Soon</a>
                        </div>
                    </div>
                </section>
                <!-- End of Members -->

                <!-- Contact -->
                <section class="kpc-contact" id="kpc-contact">
                    <div class="cntr-960">
                        <div class="kpc-tit tit-white">
                            <h3>Contact</h3>
                        </div>
                        <p class="kpc-desc tc">
                            Please fill out your contact details and message below. <br>
                            Visitors will need to consent to our Privacy Policy and Terms and Conditions to send message.<br>
                            We will get back to you as soon as we can. Thank you.
                        </p>
                        <?php 
                            echo do_shortcode('[mwform_formkey key="112"]');
                        ?>
                    </div>
                </section>
                <!-- End of Contact -->

                <!-- Access -->
                <section class="kpc-access">
                    <div class="cntr-960">
                        <div class="kpc-tit">
                            <h3>Access</h3>
                        </div>
                        <p class="kpc-desc tc">
                            <strong>K.P.C.（Kyushu Promotion Center）powered by The Company Bldg.</strong><br>
                            Villa A3, 73 Van Bao, Ngoc Khanh, Ba Dinh, Ha Noi, Vietnam<br><br>

                            <strong>K.P.C.（Kyushu Promotion Center）Wagyu Dining Ushino Kura / Showroom Bldg.</strong><br>
                            44 Van Bao, Ngoc Khanh, Ba Dinh, Ha Noi, Vietnam
                        </p>
                        <div class="kpc-access-map">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/access/img01.jpg" alt="" class="is-wide">
                        </div>
                    </div>
                </section>
                <!-- End of Access -->
            <?php
        }else{
            ?>
                <!-- News -->
                <section class="kpc-news">
                    <div class="cntr-1170">
                        <div class="kpc-news-row">
                            <div class="kpc-tit">
                                <h3>News</h3>
                            </div>
                            <div class="kpc-news-cont">
                                <?php
                                // news query
                                $news_query = new WP_Query(array('post_type'=>'news', 'post_status'=>'publish', 'posts_per_page'=>1, 'order'=> 'DESC')); ?>
                                <?php if ( $news_query->have_posts() ) : ?>

                                    <?php while ( $news_query->have_posts() ) : $news_query->the_post(); ?>
                                    <div class="kpc-news-left">
                                        <span class="kpc-news-date"><?php the_date('Y.m.d'); ?></span>
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>">Read More</a>
                                    <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                                <?php else : ?>
                                    <h4 class="cc-no-post tc"><?php _e( 'Coming Soon...' ); ?></h4>
                                <?php endif; ?>
                                
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of News -->

                <!-- Concept -->
                <section class="kpc-concept" id="kpc-concept">
                    <div class="cntr-1170">
                        <div class="kpc-tit">
                            <h3>MÔ HÌNH KINH DOANH</h3>
                        </div>
                        <div class="kpc-con-txt">
                            <h4>
                                "Tháp biểu tượng Kyushu"<br>
                                kết nối Kyushu và Hà Nội.
                            </h4>
                        </div>
                        <div class="kpc-con-cont">
                            <p>
                                Trung tâm Xúc tiến & Quảng bá vùng Kyushu được điều hành bởi Công ty The Company, một hạt nhân thông tin và mạng lưới cho các cơ quan liên quan đến Kyushu, dự kiến ​​sẽ khai trương vào ngày 2 tháng 11.<br><br>
                                Với sự hợp tác của Liên đoàn Kinh tế Kyushu, Công ty Kamichiku Group CÔNG TY CỔ PHẦN KAMICHIKU VIỆT NAM đã ký hợp đồngvới Bộ Ngoại giao Việt Nam. Sự hợp tác này đã dẫn đến sự hợp tác với 'The Company', một thương hiệu của công ty cổ phần toàn cầu Zero-Ten Park Co., Ltd.<br><br>
                                "Tháp biểu tượng Kyushu" là Trung tâm Xúc tiến & Quảng bá vùng Kyushu của chúng tôi, nằm ở khu vực Kim Mã, trung tâm Hà Nội, bao gồm hai cơ sở thuộc sở hữu nhà nước thuộc Bộ Ngoại giao Việt Nam.<br><br>
                                Với việc sử dụng các không gian làm việc chung, các sự kiện và thông qua các không gian cộng đồng khác, chúng tôi mong muốn được hình thành mạng lưới và tạo điều kiện cho các cộng đồng tham gia trao đổi giữa Kyushu và Việt Nam.<br><br>
                                Nhờ sự liên kết chặt chẽ của chúng tôi với Liên đoàn Kinh tế Kyushu, chúng tôi có thể tận dụng mối quan hệ của Liên đoàn Kinh tế Kyushu với Bộ Kế hoạch và Đầu tư Việt Nam.
                            </p>
                        </div>
                        <div class="kpc-con-circ">
                            <div class="kpc-con-circ-row">
                                <div class="kpc-con-circ-md">
                                    <div class="kpc-con-box">
                                        <div class="kpc-con-img">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico01.svg" alt="">
                                        </div>
                                        <h4 class="kpc-con-tit">DỰ ÁN</h4>
                                        <p>
                                            Không gian làm việc được nhằm<br>
                                            thúc đẩy dự án phát triển. 
                                        </p>
                                    </div>
                                </div>
                                <div class="kpc-con-circ-md">
                                    <div class="kpc-con-box">
                                        <div class="kpc-con-img">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico02.svg" alt="">
                                        </div>
                                        <h4 class="kpc-con-tit">MẠNG LƯỚI</h4>
                                        <p>
                                            Mạng lưới mở rộng hỗ trợ các<br>
                                            dự án và doanh nghiệp.
                                        </p>
                                    </div>
                                </div>
                                <div class="kpc-con-circ-md">
                                    <div class="kpc-con-box">
                                        <div class="kpc-con-img">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico03.svg" alt="">
                                        </div>
                                        <h4 class="kpc-con-tit">KẾT NỐI</h4>
                                        <p>
                                            Kết nối rộng rãi với các công ty<br>
                                            và tổ chức liên kết với Kyushu.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of Concept -->

                <!-- Service -->
                <section class="kpc-service">
                    <div class="kpc-float-box1"></div>
                    <div class="kpc-float-box2"></div>
                    <div class="kpc-float-box3"></div>
                    <div class="kpc-float-box4"></div>
                    <div class="kpc-float-box5"></div>
                    <div class="cntr-1170">
                        <div class="kpc-service-box">
                            <div class="kpc-float-box6"></div>
                            <div class="kpc-float-box7"></div>
                            <div class="kpc-tit">
                                <h3>DỊCH VỤ</h3>
                            </div>
                            <p class="kpc-desc tc">
                                Chúng tôi có các kế hoạch khác nhau phục vụ cho nhiều phong cách làm việc và quy mô nhóm.<br>
                                Có thể đăng ký một số gói chia sẻ hàng tháng với tư cách là một cơ quan công ty.<br><br>

                                Ngoài nhiều dịch vụ miễn phí dành cho thành viên,<br>
                                chúng tôi cũng cung cấp các dịch vụ tùy chọn bổ sung.
                            </p>
                            <div class="kpc-tit-jp">
                                <h4>Có sẵn dịch vụ cho tất cả các gói.</h4>
                            </div>
                            <div class="kpc-serv-plan-box">
                                <div class="kpc-plan-row">
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico04.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">Thức uống miễn phí.</p>
                                        </div>
                                    </div>
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico05.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">Wi-Fi tốc độ cao.</p>
                                        </div>
                                    </div>
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico06.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">Phòng nghe điện thoại riêng tư.</p>
                                        </div>
                                    </div>
                                    <div class="kpc-plan-md">
                                        <div class="kpc-plan-cont">
                                            <div class="kpc-plan-img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico/ico07.svg" alt="">
                                            </div>
                                            <p class="kpc-desc">
                                                Tiếp cận các văn phòng của The Company trên toàn thế giới<br>
                                                (Chỉ các thành viên của Liên đoàn Kinh tế Kyushu)
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="kpc-btn">
                                    <a href="<?php bloginfo('url'); ?>/service-vi">PLAN LIST</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of Service -->

                <!-- Members -->
                <section class="kpc-members" id="kpc-member">
                    <div class="cntr">
                        <div class="kpc-tit">
                            <h3>THÀNH VIÊN</h3>
                        </div>
                        <p class="kpc-desc tc">
                            Giới thiệu về Trung tâm Xúc tiến & Quảng bá vùng Kyushu được điều hành bởi Công ty The Company<br>
                            của chúng tôi được cung cấp bởi những thành viên thuê The Company.
                        </p>
                        <div class="kpc-btn">
                            <a href="#">Coming Soon</a>
                        </div>
                    </div>
                </section>
                <!-- End of Members -->

                <!-- Contact -->
                <section class="kpc-contact" id="kpc-contact">
                    <div class="cntr-960">
                        <div class="kpc-tit tit-white">
                            <h3>LIÊN HỆ VỚI CHÚNG TÔI</h3>
                        </div>
                        <p class="kpc-desc tc">
                            Vui lòng điền thông tin chi tiết liên hệ của bạn và tin nhắnbên dưới. Khách truy cập sẽ cần phải đồng ý với<br>
                            Chính sách quyền riêng tư và Điều khoản và Điều kiện của chúng tôi để gửi tin nhắn.<br>
                            Chúng tôi sẽ liên hệ lại với bạn ngay khi có thể. Cảm ơn bạn.
                        </p>
                        <?php 
                            echo do_shortcode('[mwform_formkey key="147"]');
                        ?>
                    </div>
                </section>
                <!-- End of Contact -->

                <!-- Access -->
                <section class="kpc-access">
                    <div class="cntr-960">
                        <div class="kpc-tit">
                            <h3>Access</h3>
                        </div>
                        <p class="kpc-desc tc">
                            <strong>K.P.C.（Kyushu Promotion Center）powered by The Company Bldg.</strong><br>
                            Villa A3, 73 Van Bao, Ngoc Khanh, Ba Dinh, Ha Noi, Vietnam<br><br>

                            <strong>K.P.C.（Kyushu Promotion Center）Wagyu Dining Ushino Kura / Showroom Bldg.</strong><br>
                            44 Van Bao, Ngoc Khanh, Ba Dinh, Ha Noi, Vietnam
                        </p>
                        <div class="kpc-access-map">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/access/img01.jpg" alt="" class="is-wide">
                        </div>
                    </div>
                </section>
                <!-- End of Access -->
            <?php
        }
    ?>

<?php
get_footer();