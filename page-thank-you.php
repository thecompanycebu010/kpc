<?php
/**
 * The template for displaying thank you pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package KPC
 */

get_header();
?>

    <!-- Contact -->
    <section class="kpc-contact" id="kpc-contact">
        <div class="cntr-960">
            <h4 class="tc">
                お問い合わせいただき誠にありがとうございます。<br><br>
                頂いたメールには、3営業日以内に返信しております。<br>
                3営業日以降、九州プロモーションセンター powered by The Companyからの連絡がない場合は、<br>
                お手数ですがもう一度お問い合わせください。
            </h4>
        </div>
    </section>
    <!-- End of Contact -->

<?php
get_footer();