<?php
/**
 * The template for displaying news pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 * 
 * Template Name: News
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package KPC
 */

get_header();
?>

    <?php
        if(pll_current_language() == 'ja'){
            ?>
                <section class="kpc-page">
                    <div class="cntr-1024">
                        <ul class="kpc-list-news">
                            
                            <?php
                                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
                                    $temp = $wp_query;
                                    $wp_query = null;
                                    $args = array( 'post_type' => 'news', 'order'=>'DESC', 'posts_per_page' => 10, 'paged' => $paged);
                                    $wp_query = new WP_Query();
                                    $wp_query->query( $args );
                                    while ($wp_query->have_posts()) : $wp_query->the_post();
                                ?>

                                <li>
                                    <a href="<?php the_permalink(); ?>" class="kpc-news-list-item">
                                        <?php if(has_post_thumbnail()) : ?>
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="is-wide" />
                                        <?php else: ?>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/noimage.jpg" alt="<?php the_title(); ?>" class="is-wide">
                                        <?php endif; ?>
                                        <div class="kpc-list-news-cont">
                                            <span class="kpc-list-news-date"><?php the_date('Y.m.d'); ?></span>
                                            <?php 
                                                the_excerpt(); 
                                            ?>
                                        </div>
                                    </a>
                                </li>
                        
                                <?php endwhile; ?>
                        </ul>
                        <?php wp_pagination(); ?>
                    </div>
                </section>
            <?php
        }else{
            ?>
                <section class="kpc-page">
                    <div class="cntr-1024">
                        <ul class="kpc-list-news">
                            
                            <?php
                                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
                                    $temp = $wp_query;
                                    $wp_query = null;
                                    $args = array( 'post_type' => 'news', 'order'=>'DESC', 'posts_per_page' => 10, 'paged' => $paged);
                                    $wp_query = new WP_Query();
                                    $wp_query->query( $args );
                                    while ($wp_query->have_posts()) : $wp_query->the_post();
                                ?>

                                <li>
                                    <a href="<?php the_permalink(); ?>" class="kpc-news-list-item">
                                        <?php if(has_post_thumbnail()) : ?>
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="is-wide" />
                                        <?php else: ?>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/noimage.jpg" alt="<?php the_title(); ?>" class="is-wide">
                                        <?php endif; ?>
                                        <div class="kpc-list-news-cont">
                                            <span class="kpc-list-news-date"><?php the_date('Y.m.d'); ?></span>
                                            <?php 
                                                the_excerpt(); 
                                            ?>
                                        </div>
                                    </a>
                                </li>
                        
                                <?php endwhile; ?>
                        </ul>
                        <?php wp_pagination(); ?>
                    </div>
                </section>
            <?php
        }
    ?>

<?php
get_footer();