/*------------------------------
Mobile Menu (toggleClass to activate transitions)
------------------------------*/
(function($){
    const navbar = $('.navbar-toggler')

    navbar.on("click",function(e){
        e.preventDefault()
        if( $(".navbar-type").hasClass("is-toggled")) {
            $(this).removeClass("is-toggled")
            $(".navbar-type").removeClass("is-toggled")
            $('body').removeClass('ov-hidden')
        }else{
            $(this).addClass("is-toggled")
            $(".navbar-type").addClass("is-toggled")
            $('body').addClass('ov-hidden')
        }
       
        return false
    })

    let navbarH = $('.navbar').innerHeight();
    let w = $(window).width();

    if( w < 769 ){
        $('.navbar-collapse').css({
            'margin-top': navbarH
        })
        $('.kpc-card').removeClass('v2')
    }
    
    if( w < 769 ){
        $(window).resize(function(){
            
                $('.navbar-collapse').css({
                    'margin-top': navbarH
                })
                $('.kpc-card').removeClass('v2')
            
        })
    }

    // Slider
    $('#kpc-space-slider').on('initialized.owl.carousel changed.owl.carousel', function(e) {
        if (!e.namespace)  {
        return;
        }
        var carousel = e.relatedTarget;
        $('.kpc-slider-counter').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
    }).owlCarousel({
        center: true,
        items: 2,
        loop:true,
        margin:0,
        nav:true,
        autoplay: false,
        margin: 70,
        dots:false,
        navText:["<div class='kpc-nav-btn kpc-prev-slide'></div>","<div class='kpc-nav-btn kpc-next-slide'></div>"],
        responsive : {
             // breakpoint from 0 up
            0 : {
                margin: 30,
            },
            // breakpoint from 480 up
            480 : {
                margin: 30,
            },
            // breakpoint from 768 up
            768 : {
                margin: 70,
            }
        }
    });

    $('#kpc-space-slider-2').on('initialized.owl.carousel changed.owl.carousel', function(e) {
        if (!e.namespace)  {
        return;
        }
        var carousel = e.relatedTarget;
        $('.kpc-slider-counter-2').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
    }).owlCarousel({
        center: true,
        items: 2,
        loop:true,
        margin:0,
        nav:true,
        autoplay: false,
        margin: 70,
        dots:false,
        navText:["<div class='kpc-nav-btn kpc-prev-slide'></div>","<div class='kpc-nav-btn kpc-next-slide'></div>"],
        responsive : {
             // breakpoint from 0 up
            0 : {
                margin: 30,
            },
            // breakpoint from 480 up
            480 : {
                margin: 30,
            },
            // breakpoint from 768 up
            768 : {
                margin: 70,
            }
        }
    });

})(jQuery);

$(document).ready(function(){

    $(function() {
        $('.kpc-tab-content:first-child').show();
        $('.kpc-tab-link').bind('click', function(e) {
          $this = $(this);
          $tabs = $this.parent().parent().next();
          $target = $($this.data("target")); // get the target from data attribute
          $this.siblings().removeClass('active');
          $target.siblings().css("display", "none")
            $this.addClass('active');
            $target.fadeIn("1000");
         
        });
        $('.kpc-tab-link:first-child').trigger('click');
      });

    var scrollLink = $('.scroll-div a');
    // scrollLink.click(function(e){
    //     e.preventDefault();
    //     $('body,html').animate({
    //         scrollTop: $(this.hash).offset().top - 80
    //     }, 1000);
    // });
    scrollLink.on('click', function() {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.substr(1) +']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - 80
            }, 1000);
            return false;
        }
    });

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.kpc-scroll-to-top a').fadeIn();
        } else {
            $('.kpc-scroll-to-top a').fadeOut();
        }
    }); 
    
    $('.kpc-scroll-to-top a').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });

})

function isOnScreen(elem) {
	// if the element doesn't exist, abort
	if( elem.length == 0 ) {
		return;
	}
	var $window = jQuery(window)
	var viewport_top = $window.scrollTop()
	var viewport_height = $window.height()
	var viewport_bottom = viewport_top + viewport_height
	var $elem = jQuery(elem)
	var top = $elem.offset().top + 100
	var height = $elem.height()
	var bottom = top + height

	return (top >= viewport_top && top < viewport_bottom) ||
	(bottom > viewport_top && bottom <= viewport_bottom) ||
	(height > viewport_height && top <= viewport_top && bottom >= viewport_bottom)
}

jQuery( document ).ready( function() {
	window.addEventListener('scroll', function(e) {
		if( isOnScreen( jQuery( '.kpc-scroll-to-top' ) ) ) { /* Pass element id/class you want to check */
            $('.kpc-scroll-to-top').addClass('p-abs')
            $('.kpc-float-social').addClass('p-abs')
 		}else{
            $('.kpc-scroll-to-top').removeClass('p-abs')
            $('.kpc-float-social').removeClass('p-abs')
         }
	});
});

$(function() {
    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form").validate({
      // Specify validation rules
      rules: {
        // The key name on the left side is the name attribute
        // of an input field. Validation rules are defined
        // on the right side
        name: "required",
        cname: "required",
        email: {
          required: true,
          email: true
        },
        pnumber: {
          required: true,
          number: true
        },
        inquiry: "required",
        checkbox_confirm: "required"
      },
      // Specify validation error messages
      messages: {
        name: "Please enter your name",
        cname: "Please enter your company name",
        pnumber:  "Please provide a phone number",
        email: "Please enter a valid email address",
        inquiry: "Please enter your inquiry",
        checkbox_confirm: "Please accept"
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
        form.submit();
      }
    });
  });
