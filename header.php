<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package KPC
 */
	$fav = esc_attr( get_option( 'favicon_url' ) );
	$logo = esc_attr( get_option( 'logo_url' ) );
	$fb = esc_attr( get_option('fb_url') );
	$twit = esc_attr( get_option('twitter_url') );
	$insta = esc_attr( get_option('insta_url') );
	$addressUrl = nl2br(esc_attr( get_option( 'address_url' ) ));
	$phoneUrl = esc_attr( get_option( 'phone_url' ) );
	$copyright = esc_attr( get_option( 'copyright_url' ) );
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/KPC_OGP.jpg">
    <title>
		<?php
		bloginfo('name');
		echo ' | ';
        if (wp_title('', false)) {
			echo "";
        } else {
            echo bloginfo('description');
        } wp_title('');
		?>
	</title>
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/KPC_favicon.ico">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if( is_singular() && pings_open( get_queried_object() ) ): ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	
    <script src="<?php echo get_template_directory_uri()?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/assets/js/lib/jquery.validate.min.js"></script>
	<?php wp_head(); ?>
	<?php 
		$custom_css = get_option( 'theme_css' );
		if(!empty($custom_css)) {
			?>
				<?php echo '<style type="text/css">'. $custom_css. '</style> '; ?>
			<?php
		}
	?>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176141904-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-176141904-1');
	</script>
</head>
<body <?php body_class(); ?>>

<!-- Header -->
<header class="menu-header">
	<nav class="navbar" role="navigation">
		<div class="cntr">
		<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-group">
				<div class="navbar-brand">
					<a class="logo" href="<?php bloginfo('url'); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.svg" alt="">
					</a>
				</div>
				<!-- use in container class navbar-slide-right, navbar-collapse for different styling -->
				<?php
					wp_nav_menu( array(
						'theme_location'    => 'primary',
						// 'depth'             => 2,
						'container'         => 'div',
						'container_class'   => 'navbar-type navbar-slide-right',
						'menu_class'        => 'nav navbar-nav',
						// 'fallback_cb'       => 'WP_Custom_Navwalker::fallback',
						// 'walker'            => new WP_Custom_Navwalker(),
					) );
				?>
				<div class="scroll-div">
					<a href="#kpc-contact" class="kpc-mail">
						<i class="far fa-envelope"></i>
					</a>
				</div>
				<button class="navbar-toggler" type="button">
					<div class="navbar-toggler-box">
						<span class="navbar-toggler-icon"></span>
					</div>
				</button>
			</div>
		</div>
	</nav>
</header>
<!-- End of Header -->

<!-- Banner -->
<?php
	if( is_front_page() ){
		?>

			<!-- Top Page Banner -->
			<section class="kpc-top-bnr">
				<div class="cntr-960">
					<div class="bnr-top-img">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bnr/bnr-txt01.png" alt="" class="is-wide">
					</div>
				</div>
			</section>
			<!-- End of Top Page Banner -->

		<?php
	}else{
		?>
		
			<!-- Page Banner -->
				<?php
					if( is_post_type_archive('news') )
					{
						?>
							<section class="kpc-page-bnr news">
								<h2>News</h2>
							</section>
						<?php
					}else if ( is_single() && 'news' == get_post_type() ){
						?>
							<section class="kpc-page-bnr news">
								<h2>News</h2>
							</section>
						<?php
					}else{
						?>
							<section class="kpc-page-bnr <?php global $post; echo $post->post_name; ?>">
								<h2><?php the_title(); ?></h2>
							</section>
						<?php
					}
				?>
			<!-- End of Page Banner -->

		<?php
	}
?>
<!-- End of Banner -->

<!-- Social Media -->
<div class="kpc-float-social">
	<ul>
		<li class="scroll-div">
			<a href="#kpc-contact">
				<i class="far fa-envelope"></i>
			</a>
		</li>
		<li>
			<a href="https://www.facebook.com/kyushupromotioncenter/" target="__blank">
				<i class="fab fa-facebook"></i>
			</a>
		</li>
		<li>
			<a href="https://www.instagram.com/kyushupromotioncenter/" target="__blank">
				<i class="fab fa-instagram"></i>
			</a>
		</li>
	</ul>
</div>
<!-- End of Social Media -->

<!-- main -->
<main class="kpc-main">