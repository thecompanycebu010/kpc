<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package KPC
 */

get_header();
?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();        
    ?>

    <section class="kpc-page">
        <div class="cntr-1024">
            <?php the_content() ; ?>
        </div>
    </section>

    <?php
    endwhile; endif; //ends the loop
    ?>

<?php
get_footer();