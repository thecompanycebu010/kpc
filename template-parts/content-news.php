<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package KPC
 */

?>

<section class="kpc-page kpc-page-news">
    <div class="cntr-1024">
        <h4 class="kpc-news-page-date"><?php the_date('Y.m.d'); ?></h4>
        <div class="kpc-page-news-img">
            <?php if(has_post_thumbnail()) : ?>
                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="is-wide" />
            <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/noimage.jpg" alt="<?php the_title(); ?>" class="is-wide">
            <?php endif; ?>
        </div>
        <?php the_content(); ?>
        <div class="kpc-btn">
            <a href="<?php bloginfo('url'); ?>/news">Back</a>
        </div>
    </div>
</section>